export EDITOR="nvim"
export SHELL="zsh"
export TERM="xterm-256color"

export ZSH="$HOME/.local/share/oh-my-zsh"
export ZSH_CUSTOM="$HOME/.config/zsh"

export HISTFILE="$HOME/.cache/zsh/zhistory"

TIMEFMT=$'\ncmd\t%J\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P\ntotal\t%*E'

ZSH_THEME="robbyrussell"

plugins=(
	git
	vi-mode
	zsh-completions
	zsh-syntax-highlighting
)

autoload -U compinit && compinit

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
  fi

source $ZSH/oh-my-zsh.sh

# User configuration

# git my dotfiles
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

